import PresentationCard from './class/PresentationCard.js';

customElements.define('presentation-card-jdal', PresentationCard)

var presentationCardWebComponent = document.createElement('presentation-card-jdal')
presentationCardWebComponent.setAttribute('name', 'Pedro')
presentationCardWebComponent.setAttribute('lastname', 'Hernandez')
presentationCardWebComponent.setAttribute('email', 'phernan@gmail.com')

document.body.appendChild(presentationCardWebComponent)
