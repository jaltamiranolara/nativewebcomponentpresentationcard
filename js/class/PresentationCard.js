export default class PresentationCard extends HTMLElement {
    constructor () {
        super()
    }
    connectedCallback() {
        this.initPresentationCard()
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if(oldValue){
            switch (name) {
                case 'name':
                    this.handleName(newValue)
                    break;
                case 'lastname':
                    if(this.name.toLowerCase() === 'jesus'){
                        this.lastnameH1.textContent = ''
                    } else {
                        this.lastnameH1.textContent = newValue
                    }
                    break;
                case 'email':
                    this.emailH1.textContent = newValue
                    this.handleEmailForSkyline(newValue)
                    break;
            }
        }
    }

    static get observedAttributes() {
        return ['name', 'lastname', 'email'];
    }

    initPresentationCard(){
        this.nameH1 = document.createElement('h1')
        this.lastnameH1 = document.createElement('h1')
        this.emailH1 = document.createElement('h1')
        this.skylineH1 = document.createElement('h1')

        this.handleName(this.name)

        this.emailH1.textContent = this.email

        this.handleEmailForSkyline(this.email)

        this.appendChild(this.nameH1)
        this.appendChild(this.lastnameH1)
        this.appendChild(this.emailH1)
        this.appendChild(this.skylineH1)
    }

    handleName(name) {
        if(name.toLowerCase() === 'jesus'){
            this.nameH1.textContent = 'Soy yo'
            this.lastnameH1.textContent = ''
        } else {
            this.nameH1.textContent = 'Hola ' + name
            this.lastnameH1.textContent = this.lastname
        }
    }

    handleEmailForSkyline(email) {
        const emailDomain = email.split('@')[1].split('.')[0]
        if(emailDomain === 'gmail'){
            this.skylineH1.textContent = 'Eres parte de skyline'
        } else {
            this.skylineH1.textContent = ''
        }
    }

    get name (){
        return this.getAttribute('name')
    }
    
    set name (newValue) {
        this.setAttribute('name', newValue)
    }

    get lastname (){
        return this.getAttribute('lastname')
    }
    
    set lastname (newValue) {
        this.setAttribute('lastname', newValue)
    }
    get email (){
        return this.getAttribute('email')
    }
    
    set email (newValue) {
        this.setAttribute('email', newValue)
    }

} 